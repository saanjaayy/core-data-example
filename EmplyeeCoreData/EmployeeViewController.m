//
//  EmployeeViewController.m
//  EmplyeeCoreData
//
//  Created by Betrand Yella on 30/05/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import "EmployeeViewController.h"
#import "DBEmployee.h"

@interface EmployeeViewController ()

@end

@implementation EmployeeViewController

@synthesize managedObjectContext = _managedObjectContext;
@synthesize empArray = _empArray;   
@synthesize tabEmployee = _tabEmployee;


// ...  
// ... default commented code from the file template  
// ...   

- (void)viewDidLoad {   
    
    [super viewDidLoad];  
    self.title = @"Lap Times";  
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTime:)];  
    self.navigationItem.rightBarButtonItem = addButton;  
    [addButton release];  
    self.empArray = [[NSMutableArray alloc]init];
    [self fetch:nil];   
    
}   

- (void)addEmployeeDetails:(id)sender {   
    
    DBEmployee *emp = (DBEmployee *)[NSEntityDescription insertNewObjectForEntityForName:@"DBEmployee" inManagedObjectContext:_managedObjectContext];  
    [emp setEno:[NSNumber numberWithInt:[txtNo.text intValue]]];
    [emp setEname:txtName.text];
    [emp setSalary:[NSNumber numberWithDouble:[txtSal.text doubleValue]]];
    [emp setDesignation:txtDest.text];
    
    
    NSError *error;  
    
    if(![_managedObjectContext save:&error]){  
        
        //This is a serious error saying the record  
        //could not be saved. Advise the user to  
        //try again or restart the application.   
        
    }  
    
//    [_empArray insertObject:emp atIndex:0];  
//    
//    [_tabEmployee reloadData];  
    
}  


-(IBAction)segmentAction:(id)sender
{
    [self fetch:nil]; 
}

-(IBAction)switchAtion:(id)sender
{
    [self fetch:nil];
}

- (NSMutableArray *)fetchRecords {   
    
    // Define our table/entity to use  
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DBEmployee" inManagedObjectContext:_managedObjectContext];   
    
   
    
    
    // Setup the fetch request  
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];  
    [fetchRequest setEntity:entity];   
    
    
    /// SetUp Predicate 

//************** Predicate for searching raj a text ------------ (Searching)...
   /* NSPredicate *predicate = [NSPredicate 
                              predicateWithFormat:@"(ename contains[cd] 'raj')"];*/
    
//***************** Predicate with two condidtions (OR) ---------- OR condition
   
    /*NSString *name1 = @"ramesh";
    NSString *name2 = @"Developer";
    NSPredicate *predicate = [NSPredicate 
                              predicateWithFormat:@"(ename contains[cd] %@) OR (designation contains[cd] %@)", name1, name2];*/
//************************* Predicate with and Condition -------- AND Condition
    
  /*  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ename contains[cd] %@ AND designation contains[cd] %@",@"rajesh",@"developer"];*/
    
// ***********************fetching/ comparing array objects : -----IN Keyword
    
   /* NSMutableArray *arr = [NSMutableArray arrayWithObjects:@"rajesh",@"Ramesh",nil];NSPredicate *predicate = [NSPredicate 
                              predicateWithFormat:@"ename IN %@",arr];
    */
    
//**************** Fetching similar object Using ------LIKE keyword
    
    /* NSPredicate *predicate =[NSPredicate predicateWithFormat:@"ename LIKE %@", @"Ramesh"];
    
    
    [fetchRequest setPredicate:predicate];*/
    
    
    // Define how we will sort the records  
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:[self filterBy] ascending:[self ascendingOrder]];  
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];  
    [fetchRequest setSortDescriptors:sortDescriptors];  
    [sortDescriptor release];   
    
    
    
    
    // Fetch the records and handle an error  
    NSError *error;  
    NSMutableArray *mutableFetchResults = [[_managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];   
    
    
    
    
    if (!mutableFetchResults) {  
        // Handle the error.  
        // This is a serious error and should advise the user to restart the application  
    }   
    
    // Save our fetched data to an array  
    [self setEmpArray:mutableFetchResults];   
    [fetchRequest release]; 
    return mutableFetchResults;
}   

-(BOOL)ascendingOrder
{
    if([swt isOn])
    {
        return YES;
    }
    else {
        return NO;
    }
}
-(NSString *)filterBy
{
    switch (seg.selectedSegmentIndex) {
        case 0:
            return @"eno";
            break;
        case 1:
            return @"ename";
            break;
        case 2:
            return @"salary";
            break;
        case 3:
            return @"designation";
            break;
            
        default:
            break;
    }
    return 0;
}
// ...  
// ... more template comments and default method definitions  
// ...   

- (void)dealloc {  
    self.managedObjectContext = nil; 
    self.empArray = nil;  
    self.tabEmployee = nil;
    [super dealloc];  
}   


-(IBAction)delete:(id)sender
{
    
    for(NSManagedObject *managedObject in _empArray)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
        // Commit the change.
        NSError *error = nil;
        if (![_managedObjectContext save:&error]) {
            // Handle the error.
        }
       
    
    [self fetch:nil];
}

-(IBAction)save:(id)sender
{
    [self addEmployeeDetails:nil];
    [self fetch:nil];
    txtNo.text = @"";
    txtName.text = @"";
    txtSal.text = @"";
    txtDest.text = @"";
    [txtNo becomeFirstResponder];
    
    
}



-(IBAction)fetch:(id)sender
{
    [txtNo resignFirstResponder];
    [txtName resignFirstResponder];
    [txtDest resignFirstResponder];
    [txtSal resignFirstResponder];
    
    if(self.empArray)
    {
        self.empArray = nil;
    }
    NSLog(@"The fetch records array is : %d",[[self fetchRecords] count]);
    
    self.empArray = [self fetchRecords];
    NSLog(@"The emp array is : %d",[self.empArray count]);
    [_tabEmployee reloadData];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark TableView Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"_emp Array count is : %d",[_empArray count]);
    
    return [_empArray count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static int LBL_TAG1 = 12;
    static int LBL_TAG2 = 24;
    
    UILabel *lbl1 = nil;
    UILabel *lbl2 = nil;
    
    NSString *cellIdentitfier = @"cellIdentifier";
    UITableViewCell *cell = [self.tabEmployee dequeueReusableCellWithIdentifier:cellIdentitfier];
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentitfier] autorelease];
        lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(150, 5, 150, 35)];
        lbl1.tag = LBL_TAG1;
        
        [cell addSubview:lbl1];
        
        lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(220, 5, 150, 35)];
        lbl2.tag = LBL_TAG2;
        
        [cell addSubview:lbl2];
        
        
    }
    else {
        lbl1 = (UILabel *)[cell viewWithTag:LBL_TAG1];
        lbl2 = (UILabel *)[cell viewWithTag:LBL_TAG2];
        
    }
    DBEmployee *emp = [_empArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = emp.ename;
    cell.detailTextLabel.text = emp.designation;
    lbl1.text = [NSString stringWithFormat:@"%@", emp.salary];
    lbl2.text = [NSString stringWithFormat:@"%@", emp.eno];
    return cell;
}


-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Delete the managed object at the given index path.
        NSManagedObject *eventToDelete = [_empArray objectAtIndex:indexPath.row];
        [_managedObjectContext deleteObject:eventToDelete];
        
        // Update the array and table view.
        [_empArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        
        // Commit the change.
        NSError *error = nil;
        if (![_managedObjectContext save:&error]) {
            // Handle the error.
        }
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
