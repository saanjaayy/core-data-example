//
//  DBEmployee.h
//  EmplyeeCoreData
//
//  Created by Betrand Yella on 30/05/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DBEmployee : NSManagedObject

@property (nonatomic, retain) NSNumber * eno;
@property (nonatomic, retain) NSString * ename;
@property (nonatomic, retain) NSNumber * salary;
@property (nonatomic, retain) NSString * designation;

@end
