//
//  EmployeeViewController.h
//  EmplyeeCoreData
//
//  Created by Betrand Yella on 30/05/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITextField *txtNo, *txtName, *txtDest, *txtSal;
    IBOutlet UISegmentedControl *seg;
    IBOutlet UISwitch *swt;
    
}

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;  
@property (nonatomic, retain) NSMutableArray *empArray;   
@property (nonatomic, retain) IBOutlet UITableView *tabEmployee;

-(IBAction)delete:(id)sender;
-(IBAction)save:(id)sender;
-(IBAction)fetch:(id)sender;
-(IBAction)switchAtion:(id)sender;
-(IBAction)segmentAction:(id)sender;
@end
