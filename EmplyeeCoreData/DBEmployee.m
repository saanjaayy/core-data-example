//
//  DBEmployee.m
//  EmplyeeCoreData
//
//  Created by Betrand Yella on 30/05/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import "DBEmployee.h"


@implementation DBEmployee

@dynamic eno;
@dynamic ename;
@dynamic salary;
@dynamic designation;

@end
