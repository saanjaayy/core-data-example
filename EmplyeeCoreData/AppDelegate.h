//
//  AppDelegate.h
//  EmplyeeCoreData
//
//  Created by Betrand Yella on 30/05/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmployeeViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) EmployeeViewController *viewController;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
